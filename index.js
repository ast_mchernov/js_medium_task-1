"use strict";

const area = document.getElementById('area');
const generateClick = document.getElementById('generate-click-btn');
const generateClicks = document.getElementById('generate-clicks-btn');
const showResult = document.getElementById('show-results-btn');
const reset = document.getElementById('reset-btn');

function generateSqures(count) {
  for(let i = 0; i < count; i++) {
    let square = document.createElement("span");
    square.id = i;
    area.append(square);
  }
};

generateSqures(100);

let squares = document.getElementsByTagName("span");
let squaresCount = squares.length;

function getPrevClicks(square) {
  return square.innerHTML;
};

function addClick(square) {
  return +getPrevClicks(square) + 1;
};

function changeSquareColor(square) {
  for(let i = 0; i < squaresCount; i++) {
    let count = +getPrevClicks(square);

    if(count > 25 && count <= 50) {
      square.style.background = "#FCF6A9";
    } else if(count > 50 && count <= 75) {
      square.style.background = "#FCCF05";
    } else if(count > 75 && count <= 100){
      square.style.background = "#FC8505";
    } else if(count > 100){
      square.style.background = "#F50202";
    } else {
      square.style.background = "#FFFFFF";
    }
  };
};

function generateRandomClicks() {
  for(let i = 0; i < squaresCount; i++) {
    let randomSquare = Math.floor(Math.random() * Math.floor(squaresCount));
    squares[randomSquare].innerHTML = addClick(squares[randomSquare]);
    changeSquareColor(squares[i]);
  } 
};

function generateSeveralClicks() {
  for(let i = 0; i < 25; i++) {
    document.getElementById('generate-click-btn').click();
  }
};

function resetSquresClicks() {
  for(let i = 0; i < squaresCount; i++) {
    squares[i].innerHTML = '';
  } 
};

for(let i = 0; i < squaresCount; i++) {
  squares[i].addEventListener('click', function() {
    squares[i].innerHTML = addClick(squares[i]);
    changeSquareColor(squares[i]);
  });
};

generateClick.addEventListener('click', function() {
  generateRandomClicks();
});

generateClicks.addEventListener('click', function() {
  generateSeveralClicks();
});

showResult.addEventListener('click', function() {
  if(showResult.classList.contains('show')) {
    showResult.classList.remove('show');
    showResult.classList.add('hide');
    area.style.fontSize = "16px";
    showResult.innerHTML = 'Hide Results';
  } else {
    showResult.classList.remove('hide');
    showResult.classList.add('show');
    area.style.fontSize = "0px";
    showResult.innerHTML = 'Show Results';
  }
});

reset.addEventListener('click', function() {
  resetSquresClicks();
  for(let i = 0; i < squaresCount; i++) {
    changeSquareColor(squares[i]);
  };
});
